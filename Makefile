CXX = g++
CXXFLAGS = -std=c++14 -lX11 -lXft -lfontconfig -lpthread -lnana -g3 -lprotobuf
PROTOC = protoc
SRCS = main.cpp
OBJS = main.o data.pb.o
HEADERS = *.h data.pb.h


all: bin

bin: main.o $(OBJS)
	$(CXX) $^ -o $@ $(CXXFLAGS)

main.o: main.cpp $(HEADERS)
	$(CXX) $< -c -o $@ $(CXXFLAGS)

%.pb.o: %.pb.cc $(HEADERS)
	$(CXX) $< -c -o $@ $(CXXFLAGS)

%.pb.h: %.proto
	$(PROTOC) --cpp_out=. $<

%.pb.cc: %.proto
	$(PROTOC) --cpp_out=. $<

clean:
	rm -f bin *.o *.pb.cc *.pb.h .depend

.depend: $(SRCS)
	$(CXX) $^ $(CXXFLAGS) -MM > .depend
