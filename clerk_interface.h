#pragma once

#include "create_request.h"
#include "data.pb.h"


namespace clerk_ui {

class SelectRequestWindow {
public:
    SelectRequestWindow(
        const RequestList& list,
        const std::function<void(int)>& callback
    ) : requestsList(list)
      , listbox(form)
    {
        listbox.append_header("Информация о пользователе");
        for (const auto& request : requestsList.requests()) {
            listbox.at(0).append({request.text()});
        }

        listbox.events().selected(
            [
                callback
            ] (const nana::arg_listbox& arg) {
                if (!arg.item.selected()) {
                    return;
                }
                callback(arg.item.pos().item);
            }
        );

        nana::place place{form};
        place.div("<list margin=[4, 2]>");
        place.field("list") << listbox;
        place.collocate();

        form.show();
    }

    void close() {
        form.close();
    }

    void hide() {
        form.hide();
    }

private:
    const RequestList& requestsList;
    nana::form form;
    nana::listbox listbox;
};


class ActionsWindow {
public:
    ActionsWindow(
        const Inventory&,
        RequestList&
    );

    void show() {
        form.show();
    }

    void editRequest(int i);

private:
    const Inventory& inventory;
    RequestList& requests;
    nana::form form;
    nana::button createRequestButton;
    nana::button editRequestButton;
    std::unique_ptr<CreateRequestWindow> createRequestWindowPtr;
    std::unique_ptr<SelectRequestWindow> selectRequestWindow;
    std::unique_ptr<CreateRequestWindow> editRequestWindow;
};

ActionsWindow::ActionsWindow(
    const Inventory& inv,
    RequestList& requests
) : inventory(inv)
  , requests(requests)
  , createRequestButton(form)
  , editRequestButton(form)
{
    nana::place place{form};
    place.div("<vert controls arrange=[30, 30, 30] gap=10 margin=[10, 90]>");
    place.field("controls")
        << createRequestButton
        << editRequestButton;
    place.collocate();

    createRequestButton.caption("Create request");
    createRequestButton.events().click(
        [
            &form = form,
            &requests = requests,
            this
        ] (const nana::arg_click&) {
            createRequestWindowPtr = std::make_unique<CreateRequestWindow>(inventory, requests);
            form.close();
        }
    );

    editRequestButton.caption("Edit request");
    editRequestButton.events().click(
        [
            &form = form,
            &requests = requests,
            this
        ] (const nana::arg_click&) {
            selectRequestWindow = std::make_unique<SelectRequestWindow>(
                requests,
                [this] (int x) { editRequest(x); }
            );
            form.close();
        }
    );
}

void ActionsWindow::editRequest(int i) {
    editRequestWindow = std::make_unique<CreateRequestWindow>(
        inventory,
        requests
    );
    editRequestWindow->description.reset(requests.requests(i).text());
    editRequestWindow->bucket.mutable_items()->CopyFrom(requests.requests(i).items());
    editRequestWindow->bucketView.update();
    requests.mutable_requests()->erase(requests.mutable_requests()->cbegin() + i);
    selectRequestWindow->hide();
    editRequestWindow->form.events().destroy(
        [&selectRequestWindow = selectRequestWindow] (const nana::arg_destroy&) {
            selectRequestWindow->close();
        }
    );
}

} //namespace clerk_ui

