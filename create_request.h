#pragma once
#include <nana/gui.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/textbox.hpp>

#include "request.h"
#include "inventory_view.h"
#include "data.pb.h"

namespace clerk_ui {

class CreateRequestWindow {
public:
    CreateRequestWindow(
        const Inventory& inv,
        RequestList& requests
    ) : inventory(inv)
      , requests(requests)
      , form(nana::rectangle{20, 20, 800, 600})
      , inventoryView(form, inventory)
      , bucketView(form, bucket)
      , requestButton(form)
      , numTextbox(form)
      , setNumButton(form)
      , saveButton(form)
      , description(form)
    {
        nana::place place{form};
        place.div("<vert <lists gap=10> <controls margin=[2, 50] gap=10 weight=30>>");
        place.field("lists")
            << inventoryView
            << bucketView;
        place.field("controls")
            << requestButton
            << numTextbox
            << setNumButton
            << saveButton
            << description;
        place.collocate();

        requestButton.caption("Add");
        requestButton.events().click(
            [this] (const nana::arg_click&) { return onAdd(); }
        );

        numTextbox.tip_string("Quantity");

        setNumButton.caption("Set quantity");
        setNumButton.events().click(
            [
                &bucketView = bucketView,
                &bucket = bucket,
                &numTextbox = numTextbox
            ] (const nana::arg_click&) {
                const auto num = numTextbox.to_int();
                if (bucketView.selected().empty()) {
                    return;
                }
                const int i = bucketView.selected().front().item;
                bucket.mutable_items(i)->set_num(num);
                bucketView.update();
            }
        );

        inventoryView.checkable(true);
        bucketView.checkable(false);

        description.tip_string("Client info");

        saveButton.caption("Save request");
        saveButton.events().click(
            [
                &description = description,
                &bucket = bucket,
                &bucketView = bucketView,
                &requests = requests
            ] (const nana::arg_click&) {
                auto* request = requests.add_requests();
                std::string str;
                description.getline(0, str);
                request->set_text(std::move(str));
                request->mutable_items()->CopyFrom(bucket.items());
                bucket.clear_items();
                bucketView.update();
            }
        );

        form.show();
    }

    void onAdd() {
        for (size_t i = 0; i < inventoryView.at(0).size(); ++i) {
            if (inventoryView.at(0).at(i).checked()) {
                auto* item = bucket.add_items();
                *item = inventoryView.inventory.items(i);
                item->set_num(1);
            }
        }
        bucketView.update();
    }


    const Inventory& inventory;
    RequestList& requests;
    Inventory bucket;
    nana::form form;
    InventoryView inventoryView;
    InventoryView bucketView;
    nana::button requestButton;
    nana::textbox numTextbox;
    nana::button setNumButton;
    nana::button saveButton;
    nana::textbox description;
};

} //namespace clerk_ui

