#pragma once

#include <fstream>

#include "data.pb.h"


using namespace std::string_literals;

template <class T>
struct ProtobufKeeper {
public:
    template <class String>
    ProtobufKeeper(const String& filename)
      : filename(filename)
    {
        std::ifstream is(filename, std::ios::binary);
        data.ParseFromIstream(&is);
        if (!data.IsInitialized()) {
            throw std::runtime_error(
                "Couldn't read data from file "s + filename
            );
        }
    }

    ProtobufKeeper(const ProtobufKeeper&) = delete;
    ProtobufKeeper(ProtobufKeeper&&) = delete;
    ProtobufKeeper& operator=(const ProtobufKeeper&) = delete;
    ProtobufKeeper& operator=(ProtobufKeeper&&) = delete;

    ~ProtobufKeeper() {
        try {
            std::ofstream os(filename, std::ios::binary);
            data.SerializeToOstream(&os);
        } catch (std::exception ex) {
            std::cerr << "Caught exception during wring data file "
                << filename << ": " << ex.what();
        }
    }

    std::string filename;
    T data;
};

struct DataStorage {
    ProtobufKeeper<Inventory> inventory{"data/inventory.pb"};
    ProtobufKeeper<RequestList> requestList{"data/requests.pb"};
    ProtobufKeeper<OrderList> orderList{"data/orders.pb"};
};

