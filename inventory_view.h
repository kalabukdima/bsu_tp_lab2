#pragma once

#include <nana/gui.hpp>
#include <nana/gui/widgets/listbox.hpp>

#include "data.pb.h"


class InventoryView : public nana::listbox {
public:
    InventoryView(const nana::form&, const Inventory&);

    void update();

public:
    const Inventory& inventory;
};

InventoryView::InventoryView(
    const nana::form& form,
    const Inventory& inventory
) : listbox(form)
  , inventory(inventory)
{
    checkable(true);

    append_header("Наименование");
    append_header("Количество");
    append_header("Цена");

    update();
}

void InventoryView::update() {
    auto_draw(false);
    clear();

    for (const auto& item : inventory.items()) {
        at(0).append({
            item.name(),
            std::to_string(item.num()),
            std::to_string(item.price())
        });
    }
    auto_draw(true);
}

