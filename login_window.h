#pragma once
#include <functional>
#include <nana/gui.hpp>
#include <nana/gui/widgets/textbox.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/msgbox.hpp>

class LoginWindow {
public:
    LoginWindow(
        std::function<void(const std::string&, const std::string&)> onLogin
    );

    void show() {
        form.show();
    }

    void close() {
        form.close();
    }

private:
    std::function<void(const std::string&, const std::string&)> onLogin;
    nana::form form{nana::rectangle(17, 17, 400, 130)};
    nana::textbox loginTextbox{form};
    nana::textbox passwordTextbox{form};
    nana::button loginButton{form, "Login"};
};

LoginWindow::LoginWindow(
    std::function<void(const std::string&, const std::string&)> onLogin
) : onLogin(onLogin)
{
    nana::place place{form};
    place.div("<vert controls arrange=[30, 30, 30] gap=10 margin=[10, 90]>");
    place.field("controls")
        << loginTextbox
        << passwordTextbox
        << loginButton;
    place.collocate();

    loginTextbox.tip_string("login");
    loginTextbox.multi_lines(false);

    passwordTextbox.tip_string("password");
    passwordTextbox.multi_lines(false);
    passwordTextbox.mask('*');

    loginButton.events().click(
        [
            &loginTextbox = loginTextbox,
            &passwordTextbox = passwordTextbox,
            onLogin = onLogin
        ] (const nana::arg_click&) {
            std::string login;
            std::string pass;
            loginTextbox.getline(0, login);
            passwordTextbox.getline(0, pass);
            onLogin(login, pass);
        }
    );
}

