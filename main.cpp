#include <iostream>
#include <string>
#include <thread>

#include <nana/gui/widgets/listbox.hpp>

#include "login_window.h"
#include "inventory_view.h"
#include "data_storage.h"
#include "clerk_interface.h"
#include "salesman_interface.h"
#include "user_database.h"


class App {
public:
    App();

    void onLogin(
        const std::string& login,
        const std::string& password
    );

private:
    DataStorage dataStorage;
    UserDatabase userDatabase;
    LoginWindow loginWindow;
    clerk_ui::ActionsWindow clerkWindow;
};

App::App()
  : loginWindow(
        [this] (
            const std::string& login,
            const std::string& pass
        ) {
            std::thread(&App::onLogin, this, login, pass).detach();
        }
    )
  , clerkWindow(
        dataStorage.inventory.data,
        dataStorage.requestList.data
    )
{
    loginWindow.show();
    nana::exec();
}

void App::onLogin(
    const std::string& login,
    const std::string& password
) {
    User user{login, password, UserAccessType::NONE};
    if (!userDatabase.authenticate(user)) {
        nana::msgbox msgbox("Bad login");
        msgbox << "Nonexistent user";
        msgbox.show();
    }
    if (user.accessType == UserAccessType::MANAGER) {
        loginWindow.close();
    } else if (user.accessType == UserAccessType::CLERK) {
        clerkWindow.show();
        loginWindow.close();
    } else if (user.accessType == UserAccessType::SALESMAN) {
        // salesmanWindow.show();
        loginWindow.close();
    }
}


void registerUsers() {
    UserDatabase db;
    auto a = db.accounts.data.add_accounts();
    a->set_login("clerk1");
    a->set_password("1");
    a->set_type(UserAccessType::CLERK);
    a = db.accounts.data.add_accounts();
    a->set_login("salesman1");
    a->set_password("s1");
    a->set_type(UserAccessType::SALESMAN);
    a = db.accounts.data.add_accounts();
    a->set_login("manager1");
    a->set_password("1234");
    a->set_type(UserAccessType::MANAGER);
}

int main() {
    App theApp;
    return 0;
}

