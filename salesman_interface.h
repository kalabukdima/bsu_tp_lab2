#pragma once

#include "create_request.h"
#include "data.pb.h"


namespace salesman_ui {

class SelectRequestWindow {
public:
    SelectRequestWindow(
        const RequestList& list,
        const std::function<void(int)>& callback
    ) : requestsList(list)
      , listbox(form)
    {
        listbox.append_header("Информация о пользователе");
        for (const auto& request : requestsList.requests()) {
            listbox.at(0).append({request.text()});
        }

        listbox.events().selected(
            [
                callback,
                &form = form
            ] (const nana::arg_listbox& arg) {
                if (!arg.item.selected()) {
                    return;
                }
                callback(arg.item.pos().item);
                form.close();
            }
        );

        nana::place place{form};
        place.div("<list margin=[4, 2]>");
        place.field("list") << listbox;
        place.collocate();

        form.show();
    }

    void close() {
        form.close();
    }

    void hide() {
        form.close();
    }

private:
    const RequestList& requestsList;
    nana::form form;
    nana::listbox listbox;
};


class ActionsWindow {
public:
    ActionsWindow(
        const Inventory&,
        RequestList&
    );

    void show() {
        form.show();
    }

    void close() {
        form.close();
    }

    void editRequest(int i);

private:
    const Inventory& inventory;
    RequestList& requests;
    nana::form form;
    nana::button createOrderButton;
    nana::button editOrderButton;
};

ActionsWindow::ActionsWindow(
    const Inventory& inv,
    RequestList& requests
) : inventory(inv)
  , requests(requests)
  , createOrderButton(form)
  , editOrderButton(form)
{
    nana::place place{form};
    place.div("<vert controls arrange=[30, 30, 30] gap=10 margin=[10, 90]>");
    place.field("controls")
        << createOrderButton
        << editOrderButton;
    place.collocate();

    createOrderButton.caption("Create order");
    editOrderButton.caption("Edit order");
}

} //namespace salesman_ui


