#pragma once

#include <string>

#include "data.pb.h"
#include "data_storage.h"

struct User {
    std::string login;
    std::string password;
    UserAccessType accessType;
};

class UserDatabase {
public:
    ProtobufKeeper<UserAccounts> accounts{"data/users.pb"};

    bool authenticate(User& user) {
        for (const auto& account : accounts.data.accounts()) {
            if (account.login() == user.login &&
                account.password() == user.password
            ) {
                user.accessType = account.type();
                return true;
            }
        }
        return false;
    }
};

